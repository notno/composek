# composEK

elasticsearch + kibana + docker-compose/swarm

---

## why?

For when you quickly need elasticsearch and kibana available on localhost (integration testing...)

* Elasticsearch
* Kibana
* xpack enabled (with default credentials, change them)

---

# Usage

## Deployment

```bash
# compose
docker-compose up -d

# swarm
ELASTIC_VERSION=7.13.2 ELASTIC_PASSWORD=changeme docker stack deploy -c docker-compose.yml composek
```

In a few minutes:
* kibana will be reachable at http://localhost:5601
* elasticsearch will be reachable at http://localhost:9200

You will also be able to reach the services attaching your containers to the default network `composek_default` using `elasticsearch` and `kibana` as hostnames.

## Usage in integration testing

You can add this project as a gitsubmodule or just deploy it on its own.
Services will be able to reach each other through the host's `localhost`.

To add as a git submodule (in case you want to make changes specific to your project)
```bash
git submodule add git@gitlab.com:notno/composek.git
```
